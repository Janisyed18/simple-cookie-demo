

import java.io.*;  
import javax.servlet.http.*;  
  
public class SecondServlet extends HttpServlet 
{  
  
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    {  
        try
        {  
            response.setContentType("text/html");  
            try (PrintWriter out = response.getWriter()) 
            {
                Cookie ck[]=request.getCookies();
                out.print("Hello "+ck[0].getValue());
            }  
  
         }
         catch(Exception e){System.out.println(e);}  
    }  
      
  
} 